package s2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Interfata extends JFrame implements ActionListener {
    private JPanel panel;
    private JTextField text1;
    private JTextField text3;
    private JTextField text2;
    private JButton produsbutton;

    public Interfata() {
        setContentPane(panel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(1000, 700);
        setVisible(true);
        produsbutton.setText("Produsul celor 2 numere este:");
        produsbutton.addActionListener(this);
        text3.setEditable(false);

    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == produsbutton) {

            if (text1.getText().equals("") || text2.getText().equals("")) {
                JOptionPane.showMessageDialog(this, "Va rugam sa introduceti 2 factori pentru a afisa produsul");
            } else {
                double number1 = Double.parseDouble(text1.getText());
                double number2 = Double.parseDouble(text2.getText());
                double total = number1 * number2;
                text3.setText(String.valueOf(total));
            }
        }
    }

    public static void main(String[] args) {
        Interfata interfata = new Interfata();
    }
}

